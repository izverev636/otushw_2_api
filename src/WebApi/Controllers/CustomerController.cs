using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        FakeDataCustomers lstCustomers;

        public CustomerController(FakeDataCustomers customers)
        { lstCustomers = customers; }


        [HttpGet("{id:long}")]   
        public Task<Customer> GetCustomerAsync([FromRoute] long id)
        {
            //Customer customer = customerLst.FirstOrDefault(c => c.Id == id);
            Task<Customer> customer = Task.Run(()=>lstCustomers.GetAllData().FirstOrDefault(c => c.Id == id));
            if (customer == null)
            {
                throw new Exception("Customer не найден");
            }
            return customer;
        }

        [HttpPost("")]   
        public Task<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            Task<long> newCustomerId = Task.Run(() => (long)lstCustomers.AddNewData(customer));
            return newCustomerId;
        }
    }
};