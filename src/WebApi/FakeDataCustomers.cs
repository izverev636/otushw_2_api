﻿using System;
using System.Collections.Generic;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class FakeDataCustomers
    {
        List<Customer> customerLst = new List<Customer>() {
        new Customer { Id = 0, Firstname = "Ivan", Lastname = "Petrov" },
        new Customer { Id = 1, Firstname = "Sergey", Lastname = "Ivanov" },
        new Customer { Id = 2, Firstname = "Vasya", Lastname = "Pupkin" },
        new Customer { Id = 3, Firstname = "Olesay", Lastname = "Dmitrova" },
        new Customer { Id = 4, Firstname = "Olga", Lastname = "Makova"} };

        public FakeDataCustomers()
        {
        }

        public List<Customer> GetAllData()
        {
            return customerLst;
        }

        public long AddNewData(Customer customer)
        {
            long newCustomerId = customerLst.Count;
            var newCustmoer = new Customer { Id = newCustomerId, Firstname = customer.Firstname, Lastname = customer.Lastname };
            customerLst.Add(newCustmoer);
            return newCustomerId;
        }
    }
}

