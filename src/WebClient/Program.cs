﻿using System;
using System.Threading.Tasks;
using RestSharp;

namespace WebClient
{
    static class Program
    {
        static Task Main(string[] args)
        {
            MainLoop();

            return Task.Run(() => Console.WriteLine("End")); ;
        }

        private static void MainLoop()
        {
            while (true)
            {
                Console.WriteLine("Введите: \n" +
                    "1 - Получить Customer\n" +
                    "2 - Создать нового Customer");
                string choice = Console.ReadLine();
                if (choice == "1") { GetCustomer(); }
                if (choice == "2") { CreateCustomer(); }
                else { Console.WriteLine("Неверно указана команада \n\n\n"); }
            }
        }

        private static void GetCustomer()
            {
                Boolean needExit = false;
                while (!needExit)
                 { 
                    Console.WriteLine("Укажите Customer ID:\n" +
                                      "Exit - для возврата");
                    string id = Console.ReadLine();
                if (id.ToLower() == "exit") { MainLoop(); };
                int customerId;
                    if (int.TryParse(id, out customerId))
                    {
                        var client = new RestClient($"https://localhost:5001/customers/{id}");
                        var request = new RestRequest();
                        var response = client.Get<Customer>(request);
                        Console.WriteLine($"Вернулся: \n" +
                            $"\tId: {response.Id}\n" +
                            $"\tFirstName: {response.Firstname}\n" +
                            $"\tLastName: {response.Lastname}\n");
                    };

                    }
                }

        private static void CreateCustomer()
        {
            var custmoer = RandomCustomer();

            Console.WriteLine($"Сгенерирован случайный Customer:\n" +
                $"FirstName: {custmoer.Firstname}\n" +
                $"LastName: {custmoer.Lastname}\n" +
                $"Нажмите любую кнопку, для его отправки.");
            Console.ReadKey();
            var client = new RestClient($"https://localhost:5001/customers/");
            var request = new RestRequest();
            request.AddBody(custmoer);
            var response = client.Post<int>(request);
            Console.WriteLine($"ID, нового Customer: {response}");
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            var random = new Random();
            string firstname = "";
            while (firstname.Length < random.Next(5, 20))
            {
                Char c = (char)random.Next(33, 125);
                if (Char.IsLetterOrDigit(c))
                    firstname += c;
            }
            string lastname = "";
            while (lastname.Length < random.Next(5, 20))
            {
                Char c = (char)random.Next(33, 125);
                if (Char.IsLetterOrDigit(c))
                    lastname += c;
            }
            return new CustomerCreateRequest { Firstname = firstname, Lastname = lastname };
        }
    }
}